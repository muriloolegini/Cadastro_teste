package cadastro;

public class Pessoa {

    private static final int TAMANHA_CPF = 11;
    private static final int TAMANHA_CNPJ = 14;
    public enum TipoPessoa {JURIDICA, FISICA}

    public Integer codigo;
    public String nome;
    private String documento;
    public TipoPessoa tipo;

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        if (documento == null || documento.isEmpty()) {
            throw new RuntimeException("Documento não pode ser nulo ou vazio!");
        }
        if (documento.length() == TAMANHA_CPF) {
            setDocumento(documento, tipo = TipoPessoa.FISICA);
        } else if (documento.length() == TAMANHA_CNPJ) {
            setDocumento(documento, tipo = TipoPessoa.JURIDICA);
        }
        else {
            throw new RuntimeException("Documento inválido para pessoa física ou jurídica!");
        }
    }
    public void setDocumento(String documento, TipoPessoa tipo){
        this.documento = documento;
        this.tipo = tipo;
    }

    public TipoPessoa getTipo() {
        return tipo;
    }
}
