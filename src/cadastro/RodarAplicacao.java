package cadastro;

public class RodarAplicacao {
    public static void main(String[] args) {
        Endereco endereco = new Endereco();
        //endereco.cep = "78085-098";

        Cliente cliente = new Cliente();

        try {
            cliente.adicionaEndereco(endereco);
            System.out.printf("Endereço adicionado com sucesso!");
        } catch (Exception e) {
            System.err.println("Houve um erro ao adicionar o endereço: " + e.getMessage());
        }
    }
}